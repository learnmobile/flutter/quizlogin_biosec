import 'package:flutter/material.dart';
import 'local_auth.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'temp_token.dart';
import 'button_style.dart';
import 'textfield_general.dart';

import 'package:get/get.dart';
import 'habilitate.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

void main() {
  runApp(const GetMaterialApp(
    title: 'Login',
    home: MyApp(),
  ));
}

class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  AndroidOptions _getAndroidOptions() => const AndroidOptions(
        encryptedSharedPreferences: true,
      );
  final _storage = const FlutterSecureStorage();
  final String tokenKey = "TOKEN";

  final usernameController = TextEditingController();
  final passwordController = TextEditingController();
  bool activeBioAuth = false;

  habilitateAuthentication(context) async {
    // try catch
    final userId =
        await _storage.read(key: tokenKey, aOptions: _getAndroidOptions());
    http.Response response = await http.post(
      // un try catch?
      Uri.parse('http://192.168.1.8:3000/loginbio'),
      headers: <String, String>{'access-token': userId!},
    );
    redirect(response);
  }

  redirect(response) async {
    if (response.statusCode == 200) {
      tempToken.setTempToken = jsonDecode(response.body)['token'];
      Get.to(() => const Habilitation());
    } else {
      tempToken.setTempToken = jsonDecode(response.body)['token'];
      Get.showSnackbar(
        const GetSnackBar(
          message: 'Invalid Data',
          icon: Icon(Icons.password),
          duration: Duration(seconds: 3),
        ),
      );
    }
  }

  isActiveBioAuthentication() async {
    final userId =
        await _storage.read(key: tokenKey, aOptions: _getAndroidOptions());
    if (userId != null) {
      setState(() {
        activeBioAuth = true;
      });
    } else {
      setState(() {
        activeBioAuth = false;
      });
    }
  }

  requestLogin(context) async {
    http.Response response =
        await http.post(Uri.parse('http://192.168.1.8:3000/login'),
            headers: <String, String>{
              'Content-Type': 'application/json; charset=UTF-8',
            },
            body: jsonEncode(<String, String>{
              'email': usernameController.text,
              'password': passwordController.text,
            }));
    redirect(response);
  }

  @override
  void initState() {
    super.initState();
    // carga cuando ya se ha creado myapp
    WidgetsBinding.instance.addPostFrameCallback((_) => isLoggedIn());
    isActiveBioAuthentication();
  }

  isLoggedIn() {
    var tokenValue = tempToken.getTempToken;
    if (tokenValue != null) {
      Get.to(() => const Habilitation());
    } else {
      Get.to(() => const MyApp());
    }
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
            appBar: AppBar(
              title: const Text('Login Page'),
              backgroundColor: Colors.lightBlue,
            ), //Builder para trabajar con el contexto
            body: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 10),
              child: Column(
                children: [
                  const FlutterLogo(size: 100),
                  textFieldGeneral(usernameController, 'Email', false),
                  textFieldGeneral(passwordController, 'Password', true),
                  ElevatedButton(
                      style: myButtonStyle,
                      onPressed: () {
                        requestLogin(context);
                      },
                      child: const Text(
                        'Login',
                        style: TextStyle(fontSize: 20),
                      )),
                  if (activeBioAuth)
                    ElevatedButton(
                        style: myButtonStyle,
                        onPressed: () async {
                          final didAuthenticate =
                              await LocalAuth.didAuthenticate();
                          if (didAuthenticate) {
                            // ignore: use_build_context_synchronously
                            habilitateAuthentication(context);
                          }
                        },
                        child: const Text(
                          'Iniciar Sesión con Datos Biométricos',
                          style: TextStyle(fontSize: 20),
                        )),
                ],
              ),
            )));
  }
}
