import 'package:flutter/material.dart';
import 'main.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'temp_token.dart';
import 'button_style.dart';
import 'textfield_general.dart';
import 'modalbottom_general.dart';
import 'local_auth.dart';
import 'dart:convert';

class Habilitation extends StatefulWidget {
  const Habilitation({super.key});

  @override
  State<Habilitation> createState() => _Habilitation();
}

class _Habilitation extends State<Habilitation> {
  AndroidOptions _getAndroidOptions() => const AndroidOptions(
        encryptedSharedPreferences: true,
      );
  final _storage = const FlutterSecureStorage();
  final String tokenKey = "TOKEN";

  String textInButton = "";
  final usernameController = TextEditingController();
  final passwordController = TextEditingController();

  snackBarWrongData() {
    return Get.showSnackbar(
      const GetSnackBar(
        message: 'Email or Password Invalid',
        icon: Icon(Icons.password),
        duration: Duration(seconds: 3),
      ),
    );
  }

  requestWithEmailAndPass() async {
    http.Response response =
        await http.post(Uri.parse('http://192.168.1.8:3000/login'),
            headers: <String, String>{
              'Content-Type': 'application/json; charset=UTF-8',
            },
            body: jsonEncode(<String, String>{
              'email': usernameController.text,
              'password': passwordController.text,
            }));
    return response;
  }

  requestLogin(context) async {
    http.Response response = await requestWithEmailAndPass();
    if (response.statusCode == 200) {
      String secToken = jsonDecode(response.body)['token'];
      await _storage.write(
          key: tokenKey, value: secToken, aOptions: _getAndroidOptions());
      changeStatusHabilitate();
    } else {
      tempToken.setTempToken = jsonDecode(response.body)['token'];
      snackBarWrongData();
    }
  }

  requestLoginLogOut(context) async {
    http.Response response = await requestWithEmailAndPass();
    if (response.statusCode == 200) {
      await _storage.delete(key: tokenKey, aOptions: _getAndroidOptions());
      changeStatusHabilitate();
    } else {
      tempToken.setTempToken = jsonDecode(response.body)['token'];
      snackBarWrongData();
    }
  }

  habilitateAuthentication(String status) async {
    http.Response response = await http.get(
      Uri.parse('http://192.168.1.8:3000/habilitate'),
      headers: <String, String>{'access-token': tempToken.getTempToken},
    );
    if (response.statusCode == 200) {
      // keep token temporary in global variable
      final didAuthenticate = await LocalAuth.didAuthenticate();
      if (didAuthenticate) {
        usernameController.text = "";
        passwordController.text = "";
        if (status == "Habilitar") {
          // ignore: use_build_context_synchronously
          buildModalBottomSheet(
              context: context,
              usernameController: usernameController,
              passwordController: passwordController,
              usernameLabel: 'Email',
              passwordLabel: 'Password',
              titleText: 'Habilitar inicio de sesión con huella',
              // ignore: use_build_context_synchronously
              functionCallback: () => requestLogin(context));
        } else {
          // ignore: use_build_context_synchronously
          showModalBottomSheet<void>(
            context: context,
            builder: (context) {
              return Container(
                height: 500,
                color: Colors.lightBlue[50],
                child: Column(
                  children: [
                    const Text('Deshabilitar inicio de sesión con huella'),
                    const SizedBox(
                      height: 10,
                    ),
                    Column(children: [
                      TextField(
                          controller: usernameController,
                          decoration: const InputDecoration(
                            border: OutlineInputBorder(),
                            labelText: "Email",
                          )),
                      TextField(
                          controller: passwordController,
                          obscureText: true,
                          enableSuggestions: false,
                          autocorrect: false,
                          decoration: const InputDecoration(
                              border: OutlineInputBorder(),
                              labelText: "Password")),
                      ElevatedButton(
                          style: myButtonStyle,
                          onPressed: () {
                            requestLoginLogOut(context);
                            Get.back();
                          },
                          child: const Text(
                            'Login',
                            style: TextStyle(fontSize: 20),
                          )),
                    ])
                  ],
                ),
              );
            },
          );
        }
      }
    } else {
      tempToken.setTempToken = null;
      Get.to(() => const MyApp());
    }
  }

  changeStatusHabilitate() async {
    // modificarlo por secure
    SharedPreferences oSharePrefs = await SharedPreferences.getInstance();
    bool? habilitate = oSharePrefs.getBool('habilitated');
    if (habilitate == null || habilitate == false) {
      habilitate = true;
    } else {
      habilitate = false;
    }
    await oSharePrefs.setBool('habilitated', habilitate);
    text();
  }

  // help to change text
  text() async {
    // todo secure preferences
    SharedPreferences oSharePrefs = await SharedPreferences.getInstance();
    bool? habilitate = oSharePrefs.getBool('habilitated');
    if (habilitate == null || habilitate == false) {
      setState(() {
        textInButton = "Habilitar";
      });
    } else {
      setState(() {
        textInButton = "Deshabilitar";
      });
    }
  }

  @override
  void initState() {
    super.initState();
    text();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
            appBar: AppBar(
              title: const Text('Habilitation Page'),
              backgroundColor: Colors.lightBlue,
            ), //Builder para trabajar con el contexto
            body: Center(
              child: Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 8, vertical: 10),
                child: Column(
                  children: [
                    const Text("Habilitar login con datos biométricos"),
                    const SizedBox(
                      height: 15,
                    ),
                    ElevatedButton(
                        style: myButtonStyle,
                        onPressed: () {
                          habilitateAuthentication(textInButton);
                        },
                        child: Text(
                          textInButton,
                          style: const TextStyle(fontSize: 20),
                        )),
                    ElevatedButton(
                        style: myButtonStyle,
                        onPressed: () {
                          // todo limpiar tempToken
                          tempToken.setTempToken = null;
                          Get.to(() => const MyApp());
                        },
                        child: const Text(
                          'Log Out',
                          style: TextStyle(fontSize: 20),
                        )),
                  ],
                ),
              ),
            )));
  }
}
