import 'package:flutter/material.dart';

final ButtonStyle myButtonStyle = ElevatedButton.styleFrom(
  padding: const EdgeInsets.fromLTRB(40, 0, 40, 0),
  backgroundColor: Colors.lightBlue,
  shape: RoundedRectangleBorder(
    borderRadius: BorderRadius.circular(18.0),
    side: const BorderSide(color: Colors.lightBlue),
  ),
);
