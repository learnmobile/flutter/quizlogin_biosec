import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'button_style.dart';
import 'textfield_general.dart';

Future<void> buildModalBottomSheet({
  required BuildContext context,
  required TextEditingController usernameController,
  required TextEditingController passwordController,
  required String usernameLabel,
  required String passwordLabel,
  required String titleText,
  required VoidCallback functionCallback,
}) {
  return showModalBottomSheet<void>(
    context: context,
    builder: (context) {
      return Container(
        height: 500,
        color: Colors.lightBlue[50],
        child: Column(
          children: [
            Text(titleText),
            const SizedBox(height: 10),
            Column(
              children: [
                textFieldGeneral(usernameController, usernameLabel, false),
                textFieldGeneral(passwordController, passwordLabel, true),
                ElevatedButton(
                  style: myButtonStyle,
                  onPressed: () {
                    functionCallback();
                    Get.back();
                  },
                  child: const Text(
                    'Login',
                    style: TextStyle(fontSize: 20),
                  ),
                ),
              ],
            ),
          ],
        ),
      );
    },
  );
}
