import 'package:flutter/material.dart';

TextField textFieldGeneral(
    TextEditingController controller, String labelText, bool enableSecText) {
  return TextField(
    controller: controller,
    obscureText: enableSecText,
    enableSuggestions: false,
    autocorrect: false,
    decoration: InputDecoration(
      border: const OutlineInputBorder(),
      labelText: labelText,
    ),
  );
}
