class TempToken {
  // ignore: prefer_typing_uninitialized_variables
  var tempToken;

  get getTempToken {
    return tempToken;
  }

  set setTempToken(var token) {
    tempToken = token;
  }
}

var tempToken = TempToken();
